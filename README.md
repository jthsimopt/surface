# Surface
*Surface* class. This is pro.

## Updates 

## Demo
Demo using the [Mesh2D class](https://bitbucket.org/jthsimopt/mesh2d). But any surface will work.
	
Creating a surface

	theta = [0,2*pi];
    phi = [0,2*pi];
    nxe = 8;
    nye = nxe*round(pi);
    M = Mesh2D(theta(1),theta(2),phi(1),phi(2),nxe,nye,eleType,order);
    M.PeriodicBoundary('XY');
    theta = M.P(:,1);
    phi = M.P(:,2);
	R = 1; r = 0.5*R;
    x = Xc(1)+(R+r*cos(theta)).*cos(phi);
    y = Xc(2)+(R+r*cos(theta)).*sin(phi);
    z = Xc(3)+r*sin(theta);
    X = [x,y,z];
	
Creating the surface object
	
	flipNormals = 1;
    SM = Surface(X, M.T, flipNormals);
	hSM = SM.Visualize('FaceNormals')

## Todo:
- Neighbors
- Local refinement
- Hanging nodes
- Basefunction
- Quadrature rules

## Properties
| Function Name   | Description |
|-----------------|-------------|
| `T`  | Element connectivity list, a.k.a. the topology, is an m-by-n matrix where *m* is the number of elements and *n* is the number of nodes for the perticular element type. Each element in *T* is a vertex ID. Each row of *T* contains the vertex IDs that define an element.  |
| `P`        | Points or Vertices, specified as a matrix whose columns are the x and y coordinates of the mesh points. The row numbers of *P* are the vertex IDs in the connectivity matrix *T*.|
| `nele`      | Numner of elements.
| `nnod`    | Number of nodes|
| `hSize`   | Element size defined as \\(h=\\sqrt{area}\\) for the quad and as the circumradii for the triangle.|
| `Element` | Element struct containing per element information. Contains the fiels **`FaceNormal`** and **`Area`**.|
| `SurfaceArea` | The total area of the surface. |
| `TR` | Triangulation object, see [triangulation](http://se.mathworks.com/help/matlab/ref/triangulation-class.html). If the surface is made with triangles.
| `Xm`, `Ym`, `Zm` | Geometrical midpoints of the elements. |

## Functions

| Function Name   | Description |
|-----------------|-------------|
| `Surface(P,T,flipNormals)`      | The constructor, creates the *Mesh2D* object |
| `Visualize(options)`   | Creates a visualization in an xfigure.  |


### Surface() - Description

**`SM = Surface(P,T,flipNormals)`**
Creates the *Surface* object `SM` using the points matrix `P` and connectivity matrix `T`. `flipNormals` is a scalar value (1 or 0), used to flip the normal orientation. Visualize the surface with `SM.Visualize('FaceNormals')` and adjust the `flipNormas` parameter accordingly.

### Visualize() - Description

**`h = Visualize()`**
Creates a visualization in a figure and outputs all graphical handles into the struct `h`.

Properties can be combined.

`h = Visualize('NodeNumbers')` Draws node numbers. 

`h = Visualize('ElementNumbers')` Draws element numbers.

`h = Visualize('FaceNormals',scale)` Creates a quiverplot on top of the visualization to illustrate the normal directions and orientation. The *scale* parameter is optional.

`h = Visualize('Elements',Ele)` Visualizes only the elements specified by the *Ele* vector that contains element indices.

## Surfaces

![](https://bytebucket.org/jthsimopt/surface/raw/0b094068bda2f5c5fc8f2f436cf3719d61eb664d/Graphics/Cylinder.png)

![](https://bytebucket.org/jthsimopt/surface/raw/0b094068bda2f5c5fc8f2f436cf3719d61eb664d/Graphics/sphere.png)


![](https://bytebucket.org/jthsimopt/surface/raw/0b094068bda2f5c5fc8f2f436cf3719d61eb664d/Graphics/torus.png)
