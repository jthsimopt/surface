function [SM,M] = surfSphere(R,Xc,eleType,flipNormals,order,ref)
    
    theta = [0,2*pi];
    phi = [0,pi];
    
    nxe = 2^(ref+1);
    nye = nxe;
    
    M = Mesh2D(theta(1),theta(2),phi(1),phi(2),nxe,nye,eleType,order);
    M.PeriodicBoundary('X');
    
    theta = M.P(:,1);
    phi = M.P(:,2);
    
    x = Xc(1)+R*cos(theta).*sin(phi);
    y = Xc(2)+R*sin(theta).*sin(phi);
    z = Xc(3)+R*cos(phi);
    
    X = [x,y,z];
    
    SM = Surface(X, M.T, flipNormals);
    
end

