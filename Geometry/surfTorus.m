function [SM,M] = surfTorus(R,r,Xc,eleType,flipNormals,order,ref)

    x0 = 0; x1 = 2*pi;
    y0 = 0; y1 = 2*pi;
    
    nxe = 2^(ref+1);
    nye = nxe*round(pi);
    
    M = Mesh2D(x0,x1,y0,y1,nxe,nye,eleType,order);
    M.PeriodicBoundary('XY');
    
    theta = M.P(:,1);
    phi = M.P(:,2);
    
    x = Xc(1)+(R+r*cos(theta)).*cos(phi);
    y = Xc(2)+(R+r*cos(theta)).*sin(phi);
    z = Xc(3)+r*sin(theta);
    
    X = [x,y,z];
    
    SM = Surface(X, M.T, flipNormals);
    
end

