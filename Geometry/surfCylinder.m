function [SM,M] = surfCylinder(R,L,Xc,eleType,flipNormals,order,ref)
    
    theta = [0,2*pi];
    x = [0,L];
    
    nxe = 2^(ref+1);
    nye = nxe;
    
    M = Mesh2D(x(1),x(2),theta(1),theta(2),nxe,nye,eleType,order);
    M.PeriodicBoundary('Y');
    
    x = M.P(:,1);
    theta = M.P(:,2);
    
    x = Xc(1)+x;
    y = Xc(2)+R*sin(theta);
    z = Xc(3)+R*cos(theta);
    
    X = [x,y,z];
    
    SM = Surface(X, M.T,flipNormals);
    
end

