classdef Surface < matlab.mixin.Copyable
    %UNTITLED5 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        P
        T
        FaceNormal
        Element
        SurfaceArea
        TR
        nele
        nnod
        hSize
        Xm
        Ym
        Zm
    end
    
    properties (Access = private)
        nK
    end
    
    methods
        function O = Surface(P, T, flipNormals)
            
            O.nK = size(T,2);
            O.nele = size(T,1);
            O.nnod = size(P,1);
            
            if O.nK == 3 %triP1
                if flipNormals; 
                    tt = T(:,[1,3,2]);
                    T = tt;
                end
                O.P = P;
                O.T = T;
                O.TR = triangulation(T,P);
                O.FaceNormal = O.TR.faceNormal;
                
                % Preallocating space
                element.FaceNormal = [0,0,0];
                element.Area = [0,0,0];
                O.Element = repmat(element,O.nele,1);
                O.SurfaceArea = 0;
                hSize = zeros(O.nele,1);
                for iel = 1:O.nele
                    Xe = P(T(iel,:),:);
                    O.Element(iel).Area = norm(cross(Xe(3,:)-Xe(1,:), Xe(2,:)-Xe(1,:)))/2;
                    O.Element(iel).FaceNormal = O.FaceNormal(iel,:);
                    O.SurfaceArea = O.SurfaceArea + O.Element(iel).Area;
                    
                    a = Xe(1,:)-Xe(3,:);
                    b = Xe(2,:)-Xe(3,:);
                    hSize(iel) = norm(a)*norm(b)*norm(a-b)/(2*norm( cross(a,b) ));
                end
                O.hSize = mean(hSize);
                
                nodes = O.T(:,:);
                X = O.P(:,1);
                Y = O.P(:,2);
                Z = O.P(:,3);  
                O.Xm = mean(X(nodes),2);
                O.Ym = mean(Y(nodes),2);
                O.Zm = mean(Z(nodes),2);
%                 xfigure
%                 plot3(O.Xm,O.Ym,O.Zm,'ob')
                
            elseif O.nK == 4 %quadP1
                O.P = P;
                O.T = T;
                O.nele = O.nele;
                O.SurfaceArea = 0;
                O.FaceNormal = zeros(O.nK,3);
                
                nodes = O.T(:,1:4);
                X = O.P(:,1);
                Y = O.P(:,2);
                Z = O.P(:,3);
                O.Xm = mean(X(nodes),2);
                O.Ym = mean(Y(nodes),2);
                O.Zm = mean(Z(nodes),2);
                
                % Preallocating space
                element.FaceNormal = [0,0,0];
                element.Area = [0,0,0];
                O.Element = repmat(element,O.nele,1);
%                 xfigure
%                 hold on
%                 axis equal
                hSize = zeros(O.nele,1);
                for iel = 1:O.nele
                    Xe = P(T(iel,:),:);
                    normal = cross(Xe(2,:)-Xe(1,:), Xe(4,:)-Xe(1,:));
                    if flipNormals; normal = -normal; end
                    area = norm(normal)/2;
                    hSize(iel) = area^.5;
                    normal = normal/area*2;
                    O.SurfaceArea = O.SurfaceArea + area;
                    O.FaceNormal(iel,:) = normal;
                    
                    O.Element(iel).FaceNormal = normal;
                    O.Element(iel).Area = area;
%                     patch(Xe(:,1),Xe(:,2),Xe(:,3),'c')
%                     if iel == 1; light; end
%                     Xm = mean(Xe,1);
%                     quiver3(Xm(1),Xm(2),Xm(3),normal(1),normal(2),normal(3))
                end
                O.hSize = mean(hSize);
            else
                error('Not implemented!')
            end
        end
        
        function h = Visualize(O,varargin)
            %% Check for xfigure
            if ~isempty(findall(0,'type','figure')) && ishold
                %g�r inget
            elseif exist('xfigure','file') == 2
                h.fig = xfigure; h.fig.Color = 'w';
            else
                RequiredFileMissing('xfigure', 'https://raw.githubusercontent.com/cenmir/xfigure/master/xfigure.m')
                RequiredFileMissing('xfigure_KPF', 'https://raw.githubusercontent.com/cenmir/xfigure/master/xfigure_KPF.m')
                h.fig = xfigure; h.fig.Color = 'w';
            end
            %% Data processing
            
            
           
            %% Check element type
            switch O.nK
                case 3
                    h = vizTriP1(O,h,varargin);
                case 4
                    h = vizQuadP1(O,h,varargin);
                case 6
                    
                case 9
                    
                otherwise
                    error('Wrong element type!')
            end
            
        end
        
    end
    
    %Hide some of the inherited methods from handle
    methods(Hidden)
      function lh = addlistener(varargin)
         lh = addlistener@handle(varargin{:});
      end
      function notify(varargin)
         notify@handle(varargin{:});
      end
      function delete(varargin)
         delete@handle(varargin{:});
      end
      function Hmatch = findobj(varargin)
         Hmatch = findobj@handle(varargin{:});
      end
      function p = findprop(varargin)
         p = findprop@handle(varargin{:});
      end
      function TF = eq(varargin)
         TF = eq@handle(varargin{:});
      end
      function TF = ne(varargin)
         TF = ne@handle(varargin{:});
      end
      function TF = lt(varargin)
         TF = lt@handle(varargin{:});
      end
      function TF = le(varargin)
         TF = le@handle(varargin{:});
      end
      function TF = gt(varargin)
         TF = gt@handle(varargin{:});
      end
      function TF = ge(varargin)
         TF = ge@handle(varargin{:});
      end
   end
end

function h = vizTriP1(O,h, varargin)
    if isenabled('Elements',varargin{:})
        ele = getoption('Elements', varargin{:});
        if isempty(ele); ele = 1:O.nele; end
    else
        ele = 1:O.nele;
    end

    h.patch = patch('Faces',O.T(ele,1:3),'Vertices',O.P,'FaceColor','w','FaceLighting','gouraud'); hold on;
    view(3), axis equal; h.light = light;
    
    if isenabled('FaceNormals',varargin{:})
        scale = getoption('FaceNormals', varargin{:});
        if isempty(scale) || ~isa(scale,'double'); scale = 1; end
        h.FaceNormals = quiver3(O.Xm(ele),O.Ym(ele),O.Zm(ele), O.FaceNormal(ele,1),O.FaceNormal(ele,2),O.FaceNormal(ele,3),scale,'Color','b');
    end
end

function h = vizQuadP1(O,h,varargin)

    if isenabled('Elements',varargin{:})
        ele = getoption('Elements', varargin{:});
        if isempty(ele); ele = 1:O.nele; end
    else
        ele = 1:O.nele;
    end
    
    h.patch = patch('Faces',O.T(ele,1:4),'Vertices',O.P,'FaceColor','w','FaceLighting','gouraud'); hold on;
    view(3), axis equal; h.light = light;
    
    if isenabled('FaceNormals',varargin{:})
        scale = getoption('FaceNormals', varargin{:});
        if isempty(scale) || ~isa(scale,'double'); scale = 1; end
        h.FaceNormals = quiver3(O.Xm(ele),O.Ym(ele),O.Zm(ele), O.FaceNormal(ele,1),O.FaceNormal(ele,2),O.FaceNormal(ele,3),scale,'Color','b');
    end
end

function RequiredFileMissing(filename, RemoteDestination)
    %If we're going to download a whole bunch of files, it is better to set
    % RequiredFilesDir to be a global and not have to ask the user to
    % specify a destination folder for every file...
    global RequiredFilesDir
    
    
    disp([filename,' is missing!'])
    disp(['Trying to download ',filename,' from ',RemoteDestination])
    
    
    if isempty(RequiredFilesDir)
        scriptPath = mfilename('class');
        [ScriptDir,~,~] = fileparts(scriptPath);
        DestDir = uigetdir(ScriptDir,['Select where to save ',filename,'. Make sure its either the script directory or a directory on the Path.']);
        if DestDir == 0
            error(['Failed to select folder, failed to install ',filename])
        end
        
        RequiredFilesDir = DestDir;
    end
    DestFile= [RequiredFilesDir,'/',filename];
    
    % Download the RemoteFile and save it to DestFile
    websave(DestFile,RemoteDestination);
    
    % Give up to 10 seconds for the file to show up, otherwise send error
    % message.
    tic
    while 1
        if exist('xfigure','file') == 2
            break
        end
        pause(0.1)
        t1 = toc;
        if t1 > 10
            error(['Failed to download ',filename,'! Timeout.'])
        end
    end

    
end

function rv = isenabled(mode, varargin)
    %   ISENABLED  Checks if mode exists in the cell-array varargin.
    %
    %   isenabled(mode,varargin{:}) return true or false.
    %   example:
    %
    %          varargin = {'Viz', 'ElementNumber', 'debug', [20,20]};
    %          isenabled('debug',varargin)
    %          ans =
    %               1
    %
    %   Author: Mirza Cenanovic (mirza.cenanovic@jth.hj.se)
    %   Date: 2013-05-02
    if nargin < 1
        error('No arguments')
    end
    varargin = varargin{:};

    ind = find(strcmpi(varargin,mode), 1);
    if ~isempty(ind)
        rv = 1;
    else
        rv = 0;
    end
end

function param = getoption(mode, varargin)
    varargin = varargin{:};
    ind1 = find(strcmpi(varargin,mode), 1);
    if ~isempty(ind1)
        %Errorhandling    
        if ~iscell(varargin)
            varargin = {varargin};
        end
        if ind1+1 <= length(varargin)
            param = varargin{ind1+1};
        else
    %         error(['No options are followed by the property ''', mode,''' '])
            param = [];
        end
    else
        param = [];
    end
end

function eleType = GetEleType(nodes)
    
    nK = size(nodes,2);
    
    switch nK
        case 3
            eleType = 'triP1';
        case 4
            eleType = 'quadP1';
        case 6
            eleType = 'triP2';
        case 9
            eleType = 'quadP2';
        otherwise
            error('Wrong element type!')
    end

end



